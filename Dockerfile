FROM openjdk:8-jdk-alpine

RUN apk update \
	&& apk upgrade \
	&& apk add --no-cache --update curl ca-certificates openssl git tar unzip bash gcc \
	&& adduser -D -h /home/container container

MAINTAINER FearNixx Technik, <technik@fearnixx.de>
	
USER container
ENV USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]