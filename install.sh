#!/bin/bash
# TS3Bot installation script
# (Put in Pterodactyl egg)
#
# Server Files: /mnt/server

cd /mnt/server

if [ ! -e t3serverbot.zip ];
	echo "Downloading and installing the bot framework"
	curl -Lo t3serverbot.zip https://gitlab.com/fearnixxgaming/t3serverbot/uploads/62d4f5aafc8b5163e3e55c5b841debfd/t3serverbot-0.0.52-bleeding.zip
	unzip t3serverbot.zip
	if [ ! -e t3serverbot.jar ]; then
		CANDS="$(find -type f | grep -E 't3serverbot&.jar')"
		LEN=${CANDS[@]}
		if [ $LEN -gt 0 ]; then
			cp ${CANDS[$LEN-1]} ts3serverbot.jar
		fi
	fi
fi
