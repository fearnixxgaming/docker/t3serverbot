#!/bin/bash
# /entrypoint.sh
# # Default entrypoint file from Pterodactyl # #
sleep 5

cd /home/container

# Output Current Java Version
java -version

# Find libraries and insert them into the start command
if [ -d "libraries" ]; then
    LIBS="$(find libraries -type f)"
fi
JAVA_CP=""
for LIB in ${LIBS}; do
    JAVA_CP="${JAVA_CP}:${LIB}"
done
JAVA_CP="$JAVA_CP:${T3SB_EXECUTABLE}"

printf "[DLIBS] ${JAVA_CP}\n"
printf "[DJVMARGS] ${T3SB_JVM_ARGS}\n"
printf "[DARGS] ${T3SB_ARGS}\n"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
${MODIFIED_STARTUP}